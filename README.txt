

Module
======
Reports


Description
===========
This module allows to create customized reports.


Requirements
============
Drupal 6.x


Installation
============
1. Move this folder into your modules directory.
2. Enable it from Administer >> Site building >> Modules >> Reports.


Configuration
=============
Configure it at Administer >> Site building >> Reports.


Projekt page
============
http://drupal.org/project/reports


Author & maintainer
===================
Ralf Stamm


License
=======
GNU General Public License (GPL)
